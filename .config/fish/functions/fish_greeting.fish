function fish_greeting

    if [ "$(tput cols)" -gt 75 ]
        echo ""
        echo " ██████╗ ██╗███████╗███╗   ██╗██╗   ██╗███████╗███╗   ██╗██╗   ██╗███████╗"
        echo " ██╔══██╗██║██╔════╝████╗  ██║██║   ██║██╔════╝████╗  ██║██║   ██║██╔════╝"
        echo " ██████╔╝██║█████╗  ██╔██╗ ██║██║   ██║█████╗  ██╔██╗ ██║██║   ██║█████╗  "
        echo " ██╔══██╗██║██╔══╝  ██║╚██╗██║╚██╗ ██╔╝██╔══╝  ██║╚██╗██║██║   ██║██╔══╝  "
        echo " ██████╔╝██║███████╗██║ ╚████║ ╚████╔╝ ███████╗██║ ╚████║╚██████╔╝███████╗"
        echo " ╚═════╝ ╚═╝╚══════╝╚═╝  ╚═══╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝"

    else
            echo -e "\t╔══════════════╗"
            echo -e "\t║  Bienvenue ! ║"
            echo -e "\t╚══════════════╝"
    end

    #lecture de os-release (pour récup du nom de l'OS)
    while read -la line
        set parts (string split '=' $line)
        if count $parts > 1
            set variable $parts[1]
            set value (string replace -a '"' '' $parts[2])
            set -g $variable $value
        end
    end < /etc/os-release

    echo ""
    echo -e "\tOS................ : $PRETTY_NAME"
    echo -e "\tHostname.......... : $hostname"
    echo -e "\tShell............. : fish"
    echo -e "\tUptime............ : $(uptime -p)"
    echo -e "\tProcesses......... : $(ps -e | wc -l) total running wich $(ps -u $USER | wc -l) are yours"
    echo -e "\tUsers logged...... : $(users | tr ' ' '\n' | sort -u)" # ajouter | wc -l pour avoir juste le nombre
    echo ""

end
