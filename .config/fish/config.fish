if status is-interactive
    # Commands to run in interactive sessions can go here
end

# récupération des alias
source $HOME/.config/aliasrc

# lancement du prompt starship§
starship init fish | source
